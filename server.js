require('dotenv').config();

const express = require('express');
const app = express();

const port = process.env.PORT || 3000;

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type");
 next();
}

app.use(express.json()); //Con esto asumen que el body que llega es un json y lo formatea
app.use(enableCORS);

const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');

app.listen(port);

console.log("Raquel escuchando en el puerto " + port);

//Login de un usuario
app.post('/apitechu/v1/login', authController.loginV1);

//Login de un usuario
app.post('/apitechu/v2/login', authController.loginV2);

//Logout de un usuario
app.post('/apitechu/v1/logout/:id', authController.logoutV1);

//Logout de un usuario
app.post('/apitechu/v2/logout/:id', authController.logoutV2);

//Consulta de usuarios. Con filtros Count y top
app.get('/apitechu/v1/users', userController.getUsersV1);

//Consulta de usuarios. Con filtros Count y top - V2
app.get('/apitechu/v2/users', userController.getUsersV2);

//Consulta de usuario por identificador
app.get('/apitechu/v1/users/:id', userController.getUserById);

//Consulta de usuario por identificador - V2
app.get('/apitechu/v2/users/:id', userController.getUserByIdV2);

//Consulta cuentas de usuario por identificador - V2
app.get('/apitechu/v2/account/:id', accountController.getAccountByIdV2);

//Creación de un usuario
app.post('/apitechu/v1/users', userController.createUser);

//Creación de un usuario - V2
app.post('/apitechu/v2/users', userController.createUserV2);

//Borrado por id
app.delete('/apitechu/v1/users/:id', userController.deleteUser);

//Borrado por id
app.delete('/apitechu/v2/users/:id', userController.deleteUserV2);

//Consulta de usuario por identificador
app.put('/apitechu/v1/users/:id', userController.updateUser);

//Ejemplo de acceso a las distintas formas de acceder a datos
app.post("/apitechu/v1/monstruo/:p1/:p2",
  function (req, res){
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    //para que se pueda leer el body que viene en formato json hay que añadir esta funcion arriba app.use(express.json());
    console.log(req.body);
  }
)
