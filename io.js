const fs = require('fs');

function writeUserData(data){
  console.log("writeUserData");

  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
    function(err){
      if(err){
        console.log(err);
      } else {
        console.log("Usario persistido");
      }
    }
  )

}
module.exports.writeUserData = writeUserData;
