const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechurmj10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const crypt = require('../crypt.js');

function getAccountByIdV2(req, res){
  console.log("GET /apitechu/v2/account/:id");
  console.log("id de cliente = "+ req.params.id);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");

  var query = "q={idUsuario : "+req.params.id+"}";

  console.log("query =" + query);

  httpClient.get("accounts?" + query + "&" + mLabAPIKey,
    function(err,resMLab, body){
      if(err){
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          var response = body;
        }else{
          var response = {
            "msg" : "El usuario no tiene cuentas"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}


// necesario para exponer la función y que se pueda usar dsede fuera
module.exports.getAccountByIdV2 = getAccountByIdV2;
