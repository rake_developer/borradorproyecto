const io = require('../io');
const crypt = require('../crypt.js');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechurmj10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const requestJson = require('request-json');


function loginV1(req, res){
  console.log("POST /apitechu/v1/loginV1");

  var users = require('../usuarios.json');
  var logado = false;

  for (var indiceAux in users) {
    if(users[indiceAux].email == req.body.email && users[indiceAux].pass == req.body.pass){
      users[indiceAux].logged = true;
      console.log("encontrado en posicion = " + indiceAux);
      io.writeUserData(users);
      logado = true;
      break;
    }
  }

  if (logado) {
    console.log("Usuario logado");
    res.send({"msg":"Login correcto" , "idUsuario" : users[indiceAux].id});
  }else {
    console.log("Usuario no encontrado");
    res.send({"msg":"Usuario no encontrado"});
  }

}

function loginV2(req, res){
  console.log("POST /apitechu/v2/loginV2");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");

  var query = 'q={"email" : "'+req.body.email+'"}';

  console.log("query =" + query);

  httpClient.get("users?" + query + "&" + mLabAPIKey,
    function(err,resMLab, body){
      if(err){
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
        res.send(response);
      }else{
        if(body.length > 0){
          var response = body[0];
          if(crypt.checkPassword(req.body.pass,body[0].pass)){
            console.log("Password correcta");
            var putBody = '{"$set":{"logged":true}}';

            console.log("url= "+baseMLabURL+"users?" + query + "&" + mLabAPIKey);
            httpClient.put("users?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function(errP,resMLabP, bodyP){
                if(errP){
                  var responseP = {
                    "msg" : "Error haciendo login"
                  }
                  res.status(500);
                  res.send(responseP);
                }else{
                  if(bodyP.n>0){
                    console.log("query = " + query);
                    console.log("putBody= "+ putBody);
                    console.log("Body="+bodyP[0]);
                    // console.log("resMLab=")
                    // console.log(resMLabP);
                    var responseP = {
                      "msg" : "Login correcto",
                      "idUsuario" : body[0].id
                    }

                    res.send(responseP);
                  }else{
                    var responseP = {
                      "msg" : "Usuario no encontrado"
                    }
                    res.status(404);
                    res.send(responseP);
                  }
                }
              }
            );
          }else{
            var response = {
              "msg" : "Error password incorrecta"
            }
            res.status(401);
            res.send(response);
          }
        }else{
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
          res.send(response);
        }
      }

    }
  );

}

function logoutV1(req, res){
  console.log("POST /apitechu/v1/logoutV1");
  console.log("usuario = " + req.params.id);

  var users = require('../usuarios.json');
  var hecho = false;

  for (var indiceAux in users) {
    if(users[indiceAux].id == req.params.id && users[indiceAux].logged){
      delete users[indiceAux].logged;
      console.log("encontrado en posicion = " + indiceAux);
      io.writeUserData(users);
      hecho = true;
      break;
    }
  }

  if (hecho) {
    console.log("Logout correcto");
    res.send({"msg":"Logout correcto" , "idUsuario" : users[indiceAux].id});
  }else {
    console.log("Logout incorrecto");
    res.send({"msg":"Logout incorrecto"});
  }

}

function logoutV2(req, res){
  console.log("POST /apitechu/v2/logoutV2");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");

  var query = 'q={"id" : '+req.params.id+'}';

  console.log("query =" + query);

  httpClient.get("users?" + query + "&" + mLabAPIKey,
    function(err,resMLab, body){
      if(err){
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(401);
        res.send(response);
      }else{
        if(body.length > 0){
          var response = body[0];
          var putBody = '{"$unset":{"logged":""}}';

          console.log("url= "+baseMLabURL+"users?" + query + "&" + mLabAPIKey);
          httpClient.put("users?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function(errP,resMLabP, bodyP){
                if(errP){
                  var responseP = {
                    "msg" : "Error haciendo login"
                  }
                  res.status(500);
                  res.send(responseP);
                }else{
                  if(bodyP.n>0){
                    console.log("query = " + query);
                    console.log("putBody= "+ putBody);
                    console.log("Body="+bodyP[0]);
                    // console.log("resMLab=")
                    // console.log(resMLabP);
                    var responseP = {
                      "msg" : "Logout correcto",
                      "idUsario" : body[0].id
                    }
                    res.status(500);
                    res.send(responseP);
                  }else{
                    var responseP = {
                      "msg" : "Usuario no encontrado"
                    }
                    res.status(404);
                    res.send(responseP);
                  }
                }
              }
            );

        }else{
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
          res.send(response);
        }
      }
      //res.send(response);
    }
  );

}

module.exports.loginV1 = loginV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV1 = logoutV1;
module.exports.logoutV2 = logoutV2;
