const io = require('../io');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechurmj10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const crypt = require('../crypt.js');

function getUsersV1(req, res){
  console.log("GET /apitechu/v1/users");
  console.log(req.query.$count);
  console.log(req.query.$top);

  //res.sendFile('usuarios.json', {root: __dirname}); /*para enviar el fichero completo al usuario*/
  var respuesta = {};
  var users = require('../usuarios.json'); //por defecto si no ponemos el ./ irá al node_modules a buscar

  if(req.query.$count && req.query.$count==='true'){
    respuesta.count = users.length;
  }

  respuesta.users = (req.query.$top && req.query.$top >= 0) ? users.slice(0,req.query.$top) : users;

  res.send(respuesta);
}

function getUsersV2(req, res){
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");

  httpClient.get("users?" + mLabAPIKey,
    function(err,resMLab, body){
      var response = !err ? body : {
        "msg" : "Error devolviendo usuarios"
      }
      res.send(response);
    }
  );

}

function getUserById(req, res){
  console.log("GET /apitechu/v1/users/{id}");
  console.log("id de cliente = "+ req.params.id);

  var users = require('../usuarios.json'); //por defecto si no ponemos el ./ irá al node_modules a buscar
  var usuario = {};

  users.forEach(function(item){
    if (item.id==req.params.id){
      usuario = item;
    }
  }
  )
    res.send(usuario);
}

function getUserByIdV2(req, res){
  console.log("GET /apitechu/v2/users/{id}");
  console.log("id de cliente = "+ req.params.id);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");

  var query = "q={id : "+req.params.id+"}";

  console.log("query =" + query);

  httpClient.get("users?" + query + "&" + mLabAPIKey,
    function(err,resMLab, body){
      if(err){
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          var response = body[0];
        }else{
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}

function createUser(req, res){
  console.log("POST /apitechu/v1/users");
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var users = require('../usuarios.json'); //por defecto si no ponemos el ./ irá al node_modules a buscar

  var nuevoId = 0;

  users.forEach(function(item){
      nuevoId = (item.id > nuevoId) ? item.id : nuevoId;
  })
  console.log("El id máximos es = " + nuevoId);

  var nuevoUser = {
    "id": nuevoId+1,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email
  }

  users.push(nuevoUser);
  io.writeUserData(users);
  console.log("Añadido nuevo user");
  res.send({"msg" : "Añadido nuevo usuario con id = "+(nuevoId+1)+" - First Name = " + req.body.first_name + " Last Name = "+ req.body.last_name + " y correo = "+req.body.email});
}

function createUserV2(req, res){
  console.log("POST /apitechu/v2/users");
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");

  var nuevoUser = {
    "id": req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "pass" : crypt.hash(req.body.pass)
  }

  //Crear usuario
  httpClient.post("users?" + mLabAPIKey, nuevoUser,
    function(err, resMLab, body) {
      console.log("Usarios creado en MLab");
      res.status(201).send({"msg" : "Usuario guardado"})
    }
  )
  //Buscar maximo
  var query = "s={id : -1}&l=1&f={id : 1, _id:0}";

  console.log("query =" + query);

  // var maximo = -1;
  // httpClient.get("users?" + query + "&" + mLabAPIKey,
  //   var maximo = function(err,resMLab, body){
  //     if(err){
  //       var response = {
  //         "msg" : "Error obteniendo usuario"
  //       }
  //       res.status(500);
  //       res.send(response);
  //     }else{
  //       if(body.length > 0){
  //         console.log("maximo dentro="+body[0].id);
  //         return body[0].id;
  //       }else{
  //         var response = {
  //           "msg" : "Usuario no encontrado"
  //         }
  //         res.status(404);
  //         res.send(response);
  //       }
  //     }
  //     //res.send(response);
  //   }
  // );
  // console.log("maximo fuera="+maximo);



}

function deleteUser(req, res){
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("id de cliente = "+ req.params.id);

  var users = require('../usuarios.json'); //por defecto si no ponemos el ./ irá al node_modules a buscar

  var posicion = -1;
  var indiceAux = 0;

  var usuario = {};

  // Con forEach. Pensasdo para recorrer arrays enteros
  users.forEach(function(item,index){
    if (item.id==req.params.id){
      posicion = index;
    }
  })
  console.log("forEach - Fuera del bucle valor posicion = " + posicion);

  // Con for normal
  posicion = -1;
  for(var i=0; i<users.length;i++){
    if (users[i].id==req.params.id){
      posicion = i;
      break;
    }
  }
  console.log("For normal - Fuera del bucle valor posicion = " + posicion);

  // Con for(element in object). CUando estamos recorriendo objetos
  posicion = -1;
  indiceAux = 0;
  for (indiceAux in users) {
    if(users[indiceAux].id == req.params.id){
      posicion = indiceAux;
      // console.log("posicion = " + posicion)
      break;
    }
  }
  console.log("For in - Fuera del bucle valor posicion = " + posicion);

  // Con for(element of object) - Con versión básica de obtener la posicion.
  posicion = -1;
  indiceAux = 0;
  for (usuario of users) {
    if(usuario.id == req.params.id){
      posicion = indiceAux;
      break;
    }
    indiceAux++;
  }

  // Con for(element of object) - Con versión avanzada de obtener la posicion
  for (var [index,usuario] of users.entries()) {
    if(usuario.id == req.params.id){
      posicion = index;
      break;
    }
  }
  console.log("For of - Fuera del bucle valor posicion = " + posicion);

  // Con findIndex
  function getPosicionPorId(element,index){
    return element.id==req.params.id;
  }

  posicion = users.findIndex(getPosicionPorId);

  console.log("findIndex - Fuera del bucle valor posicion = " + posicion);

  if(posicion>=0){
    users.splice(posicion,1); //Borra 1 elemento a partir de posición
    io.writeUserData(users);
  }
  var msg = (posicion>=0) ? "Usuario borrado" : "Usuario no encontrado";
  console.log(msg);
  res.send({"msg":msg});

}

function deleteUserV2(req, res){
  console.log("DELETE /apitechu/v2/users/{id}");
  console.log("id de cliente = "+ req.params.id);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");

  var query = "q={id : "+req.params.id+"}";

  console.log("query =" + query);

  httpClient.delete("users?" + query + "&" + mLabAPIKey,
    function(err,resMLab, body){
      if(err){
        var response = {
          "msg" : "Error obteniendo borrando usuario"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          var response = body[0];
        }else{
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}

function updateUser(req, res){
  console.log("PUT /apitechu/v1/users/{id}");
  console.log("id de cliente = "+ req.params.id);

  var users = require('../usuarios.json'); //por defecto si no ponemos el ./ irá al node_modules a buscar
  var posicion = -1;

  for (var indiceAux in users) {
    if(users[indiceAux].id == req.params.id){
      posicion = indiceAux;
      console.log("posicion = " + posicion)
      break;
    }
  }

    if (posicion >= 0){
      users[posicion].first_name = req.body.first_name;
      users[posicion].last_name = req.body.last_name;
      users[posicion].email = req.body.email;
      io.writeUserData(users);
      console.log("Usuario modificado");
      res.send({"msg":"Usuario modificado"});
    }else{
      console.log("Usuario no encontrado");
      res.send({"msg":"Usuario no encontrado"});
    }

}

// necesario para exponer la función y que se pueda usar dsede fuera
module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserById = getUserById;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUser = createUser;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUser = deleteUser;
module.exports.deleteUserV2 = deleteUserV2;
module.exports.updateUser = updateUser;
